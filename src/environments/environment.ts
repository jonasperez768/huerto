// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyBonmBaO1Kghm8HbB940P8qxD2SqOm31rw",
  authDomain: "huertos-704ad.firebaseapp.com",
  databaseURL: "https://huertos-704ad-default-rtdb.firebaseio.com",
  projectId: "huertos-704ad",
  storageBucket: "huertos-704ad.appspot.com",
  messagingSenderId: "948489515245",
  appId: "1:948489515245:web:81a37bf93f40047fc0dce4",
  measurementId: "G-3CKZGJDGZY"
  },
  
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
