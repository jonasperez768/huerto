import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-porfile',
  templateUrl: './main-porfile.component.html',
  styleUrls: ['./main-porfile.component.css']
})
export class MainPorfileComponent implements OnInit {
  inputDeshabilitado: boolean = true;
  dataForm: FormGroup;

  constructor(private authService: FirebaseService, private router: Router, private formBuilder: FormBuilder) {
    this.dataForm = this.formBuilder.group({
      nombre: ['',Validators.required],
      email: ['',Validators.required],
      
      // Agrega más campos según tu estructura de datos
    });
    
   }

  logout(): void {
    this.authService.logout()
      .then(() => {
        // Redirige a la página de inicio o realiza alguna acción adicional
        this.router.navigate(['/login']);
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  async actualizarDatosUsuario(): Promise<void> {
    try {
      if (this.dataForm.valid) {
        const nuevosDatos = {
          nombre: this.dataForm.get('nombre')?.value,
          //email:this.dataForm.get('email')?.value
          // Agrega más campos según sea necesario
        };
  
        await this.authService.actualizarDatosUsuario(nuevosDatos);
        console.log('Datos del usuario actualizados correctamente');
      }
    } catch (error) {
      console.error('Error al actualizar los datos del usuario desde el formulario:', error);
      // Maneja el error según tus necesidades
    }
  }
  





  ngOnInit(): void {
    // Obtén los datos del usuario actual y carga los datos existentes en el formulario
    this.authService.getDatosUsuario().subscribe(user => {
      this.dataForm.patchValue({
        nombre: user.nombre,
        email: user.email
        // Agrega más campos según sea necesario
      });
    });
   

  }

}
