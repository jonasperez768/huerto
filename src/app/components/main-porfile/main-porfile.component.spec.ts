import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPorfileComponent } from './main-porfile.component';

describe('MainPorfileComponent', () => {
  let component: MainPorfileComponent;
  let fixture: ComponentFixture<MainPorfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPorfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainPorfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
