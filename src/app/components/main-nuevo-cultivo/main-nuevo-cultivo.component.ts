import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cultivo } from 'src/app/models/cultivo';
///import Swal from 'sweetalert2';
@Component({
  selector: 'app-main-nuevo-cultivo',
  templateUrl: './main-nuevo-cultivo.component.html',
  styleUrls: ['./main-nuevo-cultivo.component.css']
})
export class MainNuevoCultivoComponent implements OnInit {
  cultivoForm: FormGroup;
  imagenSeleccionada: File | null = null;
  imagenUrl: string | null = null; // Nueva propiedad para almacenar la URL de la imagen

  constructor(private fb: FormBuilder, private authService: FirebaseService, private router: Router) { 
    this.cultivoForm = this.fb.group({
      fecha: ['', Validators.required],
      nombreComun: ['', Validators.required],
      nombreCientifico: ['', Validators.required],
      descripcion: ['', Validators.required],
      crecimiento: ['', Validators.required],
      luzt: ['', Validators.required],
      tierra: ['', Validators.required],
      ubicacion: ['', Validators.required],
      substrato: ['', Validators.required],
    });
   }
   onFileSelected(event: any): void {
    const file = event.target.files[0];
    this.imagenSeleccionada = file;

    // Obtener la URL de la imagen seleccionada para mostrarla
    if (this.imagenSeleccionada) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagenUrl = reader.result as string;
      };
      reader.readAsDataURL(this.imagenSeleccionada);
    } else {
      this.imagenUrl = null;
    }
  }
  logout(): void {
    this.authService.logout()
      .then(() => {
        // Redirige a la página de inicio o realiza alguna acción adicional
        this.router.navigate(['/login']);
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  guardar() {
    if (this.cultivoForm.valid) {
      const cultivoData: Cultivo = this.cultivoForm.value;
      this.authService.codCultivo(cultivoData,this.imagenSeleccionada)
        .then(() => {
         
          console.log('Cultivo registrado exitosamente');
          // Puedes realizar otras acciones después de guardar en Firebase
        })
        .catch((error) => {
          console.error('Error al registrar el cultivo:', error);
        });
    }
  }
  // Método para resetear el formulario
  resetearFormulario(): void {
    this.cultivoForm.reset();
  }

  ngOnInit(): void {
  }

}
