import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNuevoCultivoComponent } from './main-nuevo-cultivo.component';

describe('MainNuevoCultivoComponent', () => {
  let component: MainNuevoCultivoComponent;
  let fixture: ComponentFixture<MainNuevoCultivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainNuevoCultivoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainNuevoCultivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
