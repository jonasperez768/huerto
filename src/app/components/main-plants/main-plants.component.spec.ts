import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPlantsComponent } from './main-plants.component';

describe('MainPlantsComponent', () => {
  let component: MainPlantsComponent;
  let fixture: ComponentFixture<MainPlantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPlantsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainPlantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
