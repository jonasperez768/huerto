import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { Cultivo } from 'src/app/models/cultivo';
import { Etapa } from 'src/app/models/etapa';
import { User} from 'src/app/models/user';
import { Observable } from 'rxjs';
import { Chart, registerables } from 'chart.js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
(pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-main-plants',
  templateUrl: './main-plants.component.html',
  styleUrls: ['./main-plants.component.css']
})
export class MainPlantsComponent implements OnInit {
  filtroBusqueda: string = '';
  cultivoActualIndex = 0; // Índice del cultivo actual
  public chart: Chart;
  data: any; // Asegúrate de que la propiedad 'data' está declarada
  user: User;
  cultivos$: Observable<Cultivo[]>;
  etapas$: Observable<Etapa[]>;
  currentPage = 1;
  itemsPerPage = 1;
  totalPages: number;
  Page = 1;
  itemsPage = 5;
  
  cultivos: Cultivo[] = [];
  etapas: Etapa[] = [];
  cultivoForm: FormGroup;
  etapaForm: FormGroup;

  constructor(private fb: FormBuilder,private authService: FirebaseService, private router: Router) { 
    this.cultivoForm = this.fb.group({
      fecha: ['', Validators.required],
      nombreComun: ['', Validators.required],
      nombreCientifico: ['', Validators.required],
      descripcion: ['', Validators.required],
      crecimiento: ['', Validators.required],
      luz: ['', Validators.required],
      tierra: ['', Validators.required],
      ubicacion: ['', Validators.required],
      substrato: ['', Validators.required],
       // Ejemplo de un campo para la imagen
      // imagen: ['', ]
    });

    this.etapaForm = this.fb.group({
      fecha: ['', Validators.required],
      idcultivo: ['', Validators.required],
      nombreEtapa: ['', Validators.required],
      observaciones: ['', Validators.required],
      medidas: ['', Validators.required],
      horasCalor: ['', Validators.required],
      tierra: ['', Validators.required],
      ubicacion: ['', Validators.required],
      substrato: ['', Validators.required],
    });
    
    
  }
  generarPDF(etapa:Etapa, user:User): void {
    // Ruta local de la imagen
    const imagePath = 'assets/img/itcuautla.png'; // Ajusta la ruta según la ubicación de tu imagen local
    const imagePath1 = 'assets/img/Logo.png'; // Ajusta la ruta según la ubicación de tu imagen local
    const img = 'assets/img/IoT PlantMate.png';

    // Leer la imagen como base64
    this.convertImageToBase64(img, (base64Image2) => {
    this.convertImageToBase64(imagePath1, (base64Image1) => {
    this.convertImageToBase64(imagePath, (base64Image) => {
      const docDefinition = {
        background: {
          image: base64Image2,
          width: 400, // Ancho de la imagen de fondo en puntos
          height: 560, // Alto de la imagen de fondo en puntos
          alignment: 'center',
          fit: [595.28, 841.89], // Ajusta al tamaño de la página A4
          absolutePosition: {x: 22, y: 190 }, // Baja la imagen de fondo
          opacity: 0.4 // Opacidad de la imagen de fondo
      },
        content: [
          {
            columns: [
              { image: base64Image1, width: 100, alignment: 'left',  }, // Agregar la primera imagen a la columna izquierda
              { image: base64Image, width: 100, alignment: 'right', absolutePosition:  {y: 30} }, // Agregar la segunda imagen a la columna derecha
            ]
          },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 3], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'INFORMACION REGISTRADA SOBRE LA ETAPA DEL CULTIVO', alignment: 'center' },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 10], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'Usuario:                          '+user.nombre },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 3], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'ID DEL CULTIVO:           '+ etapa.id },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 3], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'Fecha de emision:        '+etapa.fecha },
          { canvas: [{ type: 'line', x1: 0, y1: 10, x2: 500, y2: 10, lineWidth: 2 }] }, // Objeto de línea
          { text: '', // Texto vacío para crear espacio
            margin: [0, 6], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'INFORMACION DEL REPORTE ', alignment: 'center' },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 6], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'Nombre de la etapa:    '+ etapa.nombreEtapa },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 3], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'Ubicacion:                     '+ etapa.ubicacion },
          { text: '', // Texto vacío para crear espacio
            margin: [0, 3], // Margen para dejar espacio, ajusta según sea necesario
          },
          { text: 'Observaciones:             '+ etapa.observaciones },
         
        ],
        styles: {
          header: {
            fontSize: 18,
            bold: true
          }
        }
      };

      pdfMake.vfs = pdfFonts.pdfMake.vfs;
      const pdfDoc = pdfMake.createPdf(docDefinition);
      pdfDoc.open();
    });
  });
});
  }
  convertImageToBase64(path: string, callback: (base64: string) => void): void {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      const reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result as string);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', path);
    xhr.responseType = 'blob';
    xhr.send();
  }

  logout(): void {
    this.authService.logout()
      .then(() => {
        // Redirige a la página de inicio o realiza alguna acción adicional
        this.router.navigate(['/login']);
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  actualizarCultivos(): void {
    this.cultivos$ = this.authService.getCultivo();

    this.cultivos$.subscribe(cultivos => {
      this.totalPages = Math.ceil(cultivos.length / this.itemsPerPage);
      // Verificar si currentPage es mayor que totalPages y ajustar
      if (this.currentPage > this.totalPages) {
        this.currentPage = this.totalPages;
      }
    });
  }

  actualizarEtapas(): void {
    this.etapas$ = this.authService.getEtapas();

    this.etapas$.subscribe(etapas => {
      this.totalPages = Math.ceil(etapas.length / this.itemsPage);
      // Verificar si currentPage es mayor que totalPages y ajustar
      if (this.Page > this.totalPages) {
        this.Page = this.totalPages;
      }
    });
  }
  cambiar(delta: number): void {
    this.Page += delta;
    this.actualizarEtapas();
  }

  cambiarPagina(delta: number): void {
    this.currentPage += delta;
    this.actualizarCultivos();
  }

   
   cambiarCultivo(delta: number): void {
    

    const nuevoIndex = this.cultivoActualIndex + delta;

    if (nuevoIndex >= 0 && nuevoIndex < this.cultivos.length) {
      this.cultivoActualIndex = nuevoIndex;
      this.actualizarGraficas();
    }
  }

  cambiarCultivoPagina(delta: number): void {
    const nuevoIndex = this.cultivoActualIndex + delta;
    const newPage = this.currentPage + delta;
 
  
    if (nuevoIndex >= 0 && nuevoIndex < this.cultivos.length) {
      this.cultivoActualIndex = nuevoIndex;
      this.actualizarGraficas();
    } 
  }

  actualizarGraficas(): void {
    const cultivo = this.cultivos[this.cultivoActualIndex];
    // Llama a las funciones existentes para dibujar las gráficas con los datos del nuevo cultivo
  
    this.drawHumedityChart([cultivo]);
  }

  ngOnInit(): void {
    this.actualizarCultivos();
    this.authService.getCultivo().subscribe(
      (cultivos) => {
        this.cultivos = cultivos;
        console.log('Cultivos actualizados:', this.cultivos);
        this.actualizarGraficas();
      },
      (error) => console.error('Error al obtener cultivos:', error)
    );

    this.actualizarEtapas();

    this.authService.getEtapas().subscribe(
      (etapas) => {
        this.etapas = etapas;
        console.log('Etapas actualizados:', this.etapas);
      },
      (error) => console.error('Error al obtener las etapas:', error)
    );
  }

  async deleteCultivo(nuevoCultivoId: string): Promise<void> {
    try {
      await this.authService.deleteCultivo(nuevoCultivoId);
      // Actualizar la lista de etapas después de eliminar
      this.cultivos = this.cultivos.filter(cultivo => cultivo.id !== nuevoCultivoId);
    } catch (error) {
      console.error('Error al eliminar el cultivo:', error);
    }
  }

  async deleteEtapa(nuevaEtapaId: string): Promise<void> {
    try {
      await this.authService.deleteEtapa(nuevaEtapaId);
      // Actualizar la lista de etapas después de eliminar
      this.cultivos = this.cultivos.filter(etapa => etapa.id !== nuevaEtapaId);
    } catch (error) {
      console.error('Error al eliminar la etapa:', error);
    }
  }

  

  editarCultivo(cultivo: Cultivo): void {
    // Cambiar el modo de edición para el cultivo seleccionado
    cultivo.editable = true;
    this.cultivoForm.patchValue({
      fecha: cultivo.fecha,
      nombreComun: cultivo.nombreComun,
      nombreCientifico: cultivo.nombreCientifico,
      descripcion: cultivo.descripcion,
      crecimiento: cultivo.crecimiento,
      tierra: cultivo.tierra,
      ubicacion: cultivo.ubicacion,
      substrato: cultivo.substrato,
      
      // ... Otros campos ...
    });
  }

  editarEtapa(etapa: Etapa): void {
    // Cambiar el modo de edición para el cultivo seleccionado
    etapa.editable = true;
    this.etapaForm.patchValue({
      fecha: etapa.fecha,
      idcultivo: etapa.idcultivo,
      nombreEtapa: etapa.nombreEtapa,
      observaciones: etapa.observaciones,
      medidas: etapa.medidas,
      horasCalor:etapa.horasCalor,
      tierra: etapa.tierra,
      ubicacion: etapa.ubicacion,
      substrato: etapa.substrato,
      
      // ... Otros campos ...
    });
  }

  guardarEtapa(etapa: Etapa): void {
    // Obtener la etapa actualmente en edición
    const etapaEditado = { ...this.etapaForm.value };
  
    // Realizar lógica para guardar los cambios, por ejemplo, llamar a un servicio
    this.authService.actualizarEtapa(etapa.id, etapaEditado)
      .then(() => {
        // Salir del modo de edición
        etapa.editable = false;
        this.etapaForm.reset();
      })
      .catch(error => console.error('Error al actualizar la etapa:', error));
  }

  guardarCambios(cultivo: Cultivo): void {
    // Obtener el cultivo actualmente en edición
    const cultivoEditado = { ...this.cultivoForm.value };
  
    // Realizar lógica para guardar los cambios, por ejemplo, llamar a un servicio
    this.authService.actualizarCultivo(cultivo.id, cultivoEditado)
      .then(() => {
        // Salir del modo de edición
        cultivo.editable = false;
        this.cultivoForm.reset();
      })
      .catch(error => console.error('Error al actualizar el cultivo:', error));
  }
 
  // Función para dibujar la gráfica de humedad
drawHumedityChart(data: any[]) {
  const humedades = data.map(entry => entry.humedadAmbientePorcentual);
  const temperaturas = data.map(entry => entry.temperaturaC);
  const humedad = data.map(entry => entry.humedadSueloPorcentual);
  const ph = data.map(entry => entry.pH);
  console.log(ph)
  const chartData = {
    labels: ['Humedad', 'Temperatura', 'Humedad del Suelo', 'pH'],
    datasets: [
      {
        label: 'Datos',
        data: [
          humedades[0], // Humedad
          temperaturas[0], // Temperatura
          humedad[0], // Humedad del Suelo
          ph[0] // pH
        ],
        backgroundColor: [
          'rgba(75, 192, 192, 0.7)',
          'rgba(75, 192, 192, 0.7)',
          'rgba(75, 192, 192, 0.7)',
          'rgba(75, 192, 192, 0.7)'
        ],
      },
    ],
  };

  const chartOptions = {
    // opciones de configuración
  };

  const ctx = (document.getElementById('humidityChart') as HTMLCanvasElement).getContext('2d');
  const existingChart = Chart.getChart(ctx);
  if (existingChart) {
    existingChart.destroy();
  }

  Chart.register(...registerables);
  new Chart(ctx, {
    type: 'bar',
    data: chartData,
    options: chartOptions,
  });
}

}
