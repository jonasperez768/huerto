import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { take, map } from 'rxjs/operators';
//import Swal from 'sweetalert2';


@Injectable({
    providedIn: 'root',
  })
  export class EmailVerificationGuard implements CanActivate {
    constructor(private auth: AngularFireAuth, private router: Router) {}
  
    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
      return this.auth.authState.pipe(
        take(1),
        map((user) => {
          if (user && user.emailVerified) {
             // Usuario autenticado y correo electrónico verificado
          // Puedes redirigir o realizar otras acciones según tu necesidad
          console.log('Usuario autenticado y correo electrónico verificado');
            
            return true; // Usuario autenticado y correo electrónico verificado
          } else if (user) {
            // Usuario autenticado pero correo electrónico no verificado
            // Puedes redirigir a una página de verificación pendiente o realizar otras acciones
            this.router.navigate(['/verificar-correo']);
            return false;
          } else {
            // Usuario no autenticado
            // Puedes redirigir a la página de inicio de sesión o realizar otras acciones
            
            console.log('Inicia sesion para acceder');
            this.router.navigate(['/login']);
            return false;
          }
        })
      );
      
    }

  }