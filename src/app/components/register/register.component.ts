import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email = '';
  password = '';
  confirmPassword = ''; // Agrega un nuevo campo para confirmar la contraseña
  nombre = '';

  constructor(private authService: FirebaseService,private router: Router) { }

  register(): void {
    if (this.password !== this.confirmPassword) {
      // Muestra un mensaje de error si las contraseñas no coinciden
      console.error('Las contraseñas no coinciden');
      return;
    }

    this.authService.register(this.email, this.password, this.nombre)
      .then(() => {
        // Verifica si el correo electrónico está verificado
        if (!this.authService.isEmailVerified()) {
          this.router.navigate(['/verificar-correo']);
        } else {
          // Redirige a la página de bienvenida o realiza alguna acción adicional
          this.router.navigate(['/user']);
        }
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  ngOnInit(): void {
  }

}
