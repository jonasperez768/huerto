import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
//import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: FirebaseService,private router: Router) { 
    
  }
 
  email = '';
  password = '';

  login(): void {
    if (this.email.trim() === '' || this.password === '') {
      // Verifica si los campos están vacíos y muestra una alerta si es así
      
      return; // Evita que la función continúe si los campos están vacíos
    }


    this.authService.login(this.email, this.password)
      .then(() => {
        // Verifica si el correo electrónico está verificado
        if (!this.authService.isEmailVerified()) {
          this.router.navigate(['/verificar-correo']);
        }
        else {
        // Redirige a la página de bienvenida o realiza alguna acción adicional
        this.router.navigate(['/user']);
    }
    
      })
      .catch((error: any) => {
       
        console.error(error.message);
      });
  }

  ngOnInit(): void {
  }

}
