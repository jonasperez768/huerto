import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-user',
  templateUrl: './main-user.component.html',
  styleUrls: ['./main-user.component.css']
})
export class MainUserComponent implements OnInit {

  constructor(private authService: FirebaseService, private router: Router) { }

  logout(): void {
    this.authService.logout()
      .then(() => {
        // Redirige a la página de inicio o realiza alguna acción adicional
        this.router.navigate(['/login']);
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  ngOnInit(): void {
  }

}
