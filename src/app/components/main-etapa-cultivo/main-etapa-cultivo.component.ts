import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Etapa } from 'src/app/models/etapa';
//import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main-etapa-cultivo',
  templateUrl: './main-etapa-cultivo.component.html',
  styleUrls: ['./main-etapa-cultivo.component.css']
})
export class MainEtapaCultivoComponent implements OnInit {
  imagenSeleccionada: File | null = null;
  imagenUrl: string | null = null; // Nueva propiedad para almacenar la URL de la imagen



  etapaForm: FormGroup;
  etapas: Etapa[] = [];

  constructor(private fb: FormBuilder,private authService: FirebaseService, private router: Router) { 
    this.etapaForm = this.fb.group({
      fecha: ['', Validators.required],
      idcultivo: ['', Validators.required],
      nombreEtapa: ['', Validators.required],
      observaciones: ['', Validators.required],
      medidas: ['', Validators.required],
      horasCalor: ['', Validators.required],
      tierra: ['', Validators.required],
      ubicacion: ['', Validators.required],
      substrato: ['', Validators.required],
      imagenUrl:[null,Validators.required]
    });
    
   }
   onFileSelected(event: any): void {
    const file = event.target.files[0];
    this.imagenSeleccionada = file;

    // Obtener la URL de la imagen seleccionada para mostrarla
    if (this.imagenSeleccionada) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagenUrl = reader.result as string;
      };
      reader.readAsDataURL(this.imagenSeleccionada);
    } else {
      this.imagenUrl = null;
    }
  }

  logout(): void {
    this.authService.logout()
      .then(() => {
        // Redirige a la página de inicio o realiza alguna acción adicional
        this.router.navigate(['/login']);
      })
      .catch((error: any) => {
        console.error(error.message);
      });
  }

  guardar() {
    if (this.etapaForm.valid) {
      const etapaData: Etapa = this.etapaForm.value;
      this.authService.addEtapa(etapaData, this.imagenSeleccionada)
        .then(() => {
         
          console.log('Cultivo registrado exitosamente');
          // Puedes realizar otras acciones después de guardar en Firebase
        })
        .catch((error) => {
          console.error('Error al registrar el cultivo:', error);
        });
    }
  }
  
 // Método para resetear el formulario
 resetearFormulario(): void {
  this.etapaForm.reset();
}
 








  ngOnInit(): void {
    this.authService.getEtapas()
    .subscribe(etapas => this.etapas = etapas,
               error => console.error('Error al obtener etapas:', error));
    
    
    }
  

}
