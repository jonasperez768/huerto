import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainEtapaCultivoComponent } from './main-etapa-cultivo.component';

describe('MainEtapaCultivoComponent', () => {
  let component: MainEtapaCultivoComponent;
  let fixture: ComponentFixture<MainEtapaCultivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainEtapaCultivoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainEtapaCultivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
