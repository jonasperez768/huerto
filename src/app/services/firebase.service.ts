import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { User } from '../models/user';
import { Cultivo } from '../models/cultivo';
import { Etapa } from '../models/etapa';
import { Observable } from 'rxjs';
import { switchMap, filter,map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private basePath = '/users';

  constructor(private afAuth: AngularFireAuth, private db:AngularFireDatabase, private storage: AngularFireStorage  ) { }

  async register(email: string, password: string, nombre: string): Promise<void> {
    try {
      const credential = await this.afAuth.createUserWithEmailAndPassword(email, password);
      if (credential.user) {
        const user: User = {
          uid: credential.user.uid,
          email,
          nombre,
        };
        await this.db.object(`users/${credential.user.uid}`).set(user);

         // Enviar correo de verificación
         await credential.user.sendEmailVerification();
      } else {
        throw new Error('Datos de usuario no disponibles');
      }
    } catch (error: any) {
      console.error(error.message);
      throw error; // Vuelve a lanzar el error para un manejo posterior si es necesario
    }
  }// final del metodo register

  async login(email: string, password: string): Promise<void> {
    try {
      const credential = await this.afAuth.signInWithEmailAndPassword(email, password);
      if (!credential.user) {
        throw new Error('Datos de usuario no disponibles');
      }
    } catch (error: any) {
      console.error(error.message);
      throw error;
    }
  }// final del metodo login

  async logout(): Promise<void> {
    try {
      await this.afAuth.signOut();
    } catch (error: any) {
      console.error(error.message);
      throw error;
    }
  }

  async isEmailVerified(): Promise<boolean> {
    const user = await this.afAuth.currentUser;

    // Verifica si el usuario existe y si su correo electrónico está verificado
    return !!user && user.emailVerified;
  }// Final del metodo verificar

  //////////////7///////////////////////////////////////////////////////////////
  // Metodo que registra los cultivos 
  async addCultivo(cultivo: Cultivo): Promise<any> {
    try {
      const user = await this.afAuth.currentUser;
      if (user) {
        const userId = user.uid;
  
        // Genera un nuevo ID para el cultivo
        const nuevoCultivoRef = this.db.list(`${this.basePath}/${userId}/cultivos`).push({});
  
        // Obtiene el ID generado y lo asigna al campo id del modelo Cultivo, si no es null
        const nuevoCultivoId = nuevoCultivoRef.key;
        if (nuevoCultivoId) {
          cultivo.id = nuevoCultivoId;
        } else {
          throw new Error('El ID del cultivo es nulo');
        }
  
        // Ahora, guarda el cultivo en la base de datos
        await this.db.object(`${this.basePath}/${userId}/cultivos/${nuevoCultivoId}`).set(cultivo);
  
        console.log('Cultivo registrado con éxito');
      } else {
        throw new Error('Usuario no autenticado');
      }
    } catch (error: any) {
      console.error('Error al registrar el cultivo:', error);
      throw error;
    }
  }

  async actualizarCultivo(cultivoId: string, nuevosDatos: Partial<Cultivo>): Promise<void> {
    try {
      const user = await this.afAuth.currentUser;

      if (!user) {
        throw new Error('Usuario no autenticado');
      }

      const userId = user.uid;

      // Actualiza los datos del cultivo en la base de datos
      await this.db.object(`${this.basePath}/${userId}/cultivos/${cultivoId}`).update(nuevosDatos);

      console.log('Cultivo actualizado con éxito');
    } catch (error) {
      console.error('Error al actualizar el cultivo:', error);
      throw error;
    }
  }

 
  /// Metodo para registrar las etapas
  async addRegistro(etapaCultivo: Etapa, imagen: File):Promise<any>{
    try {
      const user = await this.afAuth.currentUser;
      if (user) {
        const userId = user.uid;
  
        // Genera un nuevo ID para el cultivo
        const nuevoCultivoRef = this.db.list(`${this.basePath}/${userId}/etapas`).push({});
  
        // Obtiene el ID generado y lo asigna al campo id del modelo Cultivo, si no es null
        const nuevaEtapaId = nuevoCultivoRef.key;
        if (nuevaEtapaId) {
          etapaCultivo.id = nuevaEtapaId;
          // Subir la imagen asociada al ID del cultivo y obtener la URL
          const filePath = `${this.basePath}/${userId}/etapas/${nuevaEtapaId}/${new Date().getTime()}_${imagen.name}`;
          const storageRef = this.storage.ref(filePath);
          const uploadTask = this.storage.upload(filePath, imagen);
  
          await uploadTask.snapshotChanges().toPromise();
  
          const downloadURL = await storageRef.getDownloadURL().toPromise();
          etapaCultivo.imagenUrl = downloadURL;
          
        } else {
          throw new Error('El ID del cultivo es nulo');
        }
  
        // Ahora, guarda el cultivo en la base de datos
        await this.db.object(`${this.basePath}/${userId}/etapas/${nuevaEtapaId}`).set(etapaCultivo);
  
        console.log('Cultivo registrado con éxito');
      } else {
        throw new Error('Usuario no autenticado');
      }
    } catch (error: any) {
      console.error('Error al registrar el cultivo:', error);
      throw error;
    }

  }
// Metodo para registrar las etapas
async addEtapa(etapaCultivo: Etapa, imagen: File): Promise<any> {
  try {
    const user = await this.afAuth.currentUser;
    if (user) {
      const userId = user.uid;

      // Genera un nuevo ID para el cultivo
      const nuevoCultivoRef = this.db.list(`${this.basePath}/${userId}/etapas`).push({});

      // Obtiene el ID generado y lo asigna al campo id del modelo Cultivo, si no es null
      const nuevaEtapaId = nuevoCultivoRef.key;
      if (nuevaEtapaId) {
        // Genera un número aleatorio de 4 dígitos
        const randomNum = Math.floor(100000 + Math.random() * 900000);
        // Crea el nuevo ID combinando "etp" y el número aleatorio
        const nuevoId = `etp${randomNum}`;
        // Si el nuevo ID tiene más de 10 caracteres, recorta los caracteres adicionales
        etapaCultivo.id = nuevoId.substring(0, 10);

        // Subir la imagen asociada al ID del cultivo y obtener la URL
        const filePath = `${this.basePath}/${userId}/etapas/${etapaCultivo.id}/${new Date().getTime()}_${imagen.name}`;
        const storageRef = this.storage.ref(filePath);
        const uploadTask = this.storage.upload(filePath, imagen);

        await uploadTask.snapshotChanges().toPromise();

        const downloadURL = await storageRef.getDownloadURL().toPromise();
        etapaCultivo.imagenUrl = downloadURL;
      } else {
        throw new Error('El ID del cultivo es nulo');
      }

      // Ahora, guarda el cultivo en la base de datos
      await this.db.object(`${this.basePath}/${userId}/etapas/${etapaCultivo.id}`).set(etapaCultivo);

      console.log('Cultivo registrado con éxito');
    } else {
      throw new Error('Usuario no autenticado');
    }
  } catch (error: any) {
    console.error('Error al registrar el cultivo:', error);
    throw error;
  }
}

// Método que registra los cultivos
async codCultivo(cultivo: Cultivo,imagen: File): Promise<any> {
  try {
    const user = await this.afAuth.currentUser;
    if (user) {
      const userId = user.uid;

      // Genera un nuevo ID para el cultivo
      const nuevoCultivoRef = this.db.list(`${this.basePath}/${userId}/cultivos`).push({});

      // Obtiene el ID generado y lo asigna al campo id del modelo Cultivo, si no es null
      const nuevoCultivoId = nuevoCultivoRef.key;
      if (nuevoCultivoId) {
        // Genera un número aleatorio de 4 dígitos
        const randomNum = Math.floor(100000 + Math.random() * 900000);
        // Crea el nuevo ID combinando "cult" y el número aleatorio
        const nuevoId = `cult${randomNum}`;
        // Si el nuevo ID tiene más de 10 caracteres, recorta los caracteres adicionales
        cultivo.id = nuevoId.substring(0, 10);
         // Subir la imagen asociada al ID del cultivo y obtener la URL
         const filePath = `${this.basePath}/${userId}/cultivos/${cultivo.id}/${new Date().getTime()}_${imagen.name}`;
         const storageRef = this.storage.ref(filePath);
         const uploadTask = this.storage.upload(filePath, imagen);
 
         await uploadTask.snapshotChanges().toPromise();
 
         const downloadURL = await storageRef.getDownloadURL().toPromise();
         cultivo.imagenUrl = downloadURL;
      } else {
        throw new Error('El ID del cultivo es nulo');
      }

      // Ahora, guarda el cultivo en la base de datos
      await this.db.object(`${this.basePath}/${userId}/cultivos/${cultivo.id}`).set(cultivo);

      console.log('Cultivo registrado con éxito');
    } else {
      throw new Error('Usuario no autenticado');
    }
  } catch (error: any) {
    console.error('Error al registrar el cultivo:', error);
    throw error;
  }
}

  async actualizarEtapa(etapaId: string, nuevosDatos: Partial<Etapa>): Promise<void> {
    try {
      const user = await this.afAuth.currentUser;

      if (!user) {
        throw new Error('Usuario no autenticado');
      }

      const userId = user.uid;

      // Actualiza los datos del cultivo en la base de datos
      await this.db.object(`${this.basePath}/${userId}/etapas/${etapaId}`).update(nuevosDatos);

      console.log('Etapa actualizado con éxito');
    } catch (error) {
      console.error('Error al actualizar la etapa:', error);
      throw error;
    }
  }





  getEtapas(): Observable<Etapa[]> {
    return this.afAuth.user.pipe(
      switchMap(user => {
        if (user) {
          const userId = user.uid;

          // Obtener una referencia a los datos de las etapas del usuario actual
          return this.db.list<Etapa>(`${this.basePath}/${userId}/etapas`).valueChanges();
        } else {
          throw new Error('Usuario no autenticado');
        }
      })
    );
  }
  async actualizarDatosUsuario(nuevosDatos: Partial<User>): Promise<void> {
    try {
      const user = await this.afAuth.currentUser;
  
      if (!user) {
        throw new Error('Usuario no autenticado');
      }
  
      const userId = user.uid;
  
      // Actualiza los datos del usuario en la base de datos
      await this.db.object(`${this.basePath}/${userId}`).update(nuevosDatos);
  
      console.log('Datos del usuario actualizados con éxito');
    } catch (error) {
      console.error('Error al actualizar los datos del usuario:', error);
      throw error;
    }
  }
  // Método para obtener datos del usuario
  getDatosUsuario(): Observable<any> {
    return this.afAuth.user.pipe(
      switchMap(user => {
        if (user) {
          const userId = user.uid;
          return this.db.object<User>(`${this.basePath}/${userId}`).valueChanges();
        } else {
          throw new Error('Usuario no autenticado');
        }
      })
    );
  }

  getCultivo(): Observable<any> {
    return this.afAuth.user.pipe(
      switchMap(user => {
        if (user) {
          const userId = user.uid;

          // Obtener una referencia a los datos de las etapas del usuario actual
          return this.db.list<any>(`${this.basePath}/${userId}/cultivos`).valueChanges();
        } else {
          throw new Error('Usuario no autenticado');
        }
      })
    );
  }

  async deleteCultivo(nuevoCultivoId: string): Promise<void> {
    try {
      const user = await this.afAuth.currentUser;
  
      if (!user) {
        throw new Error('Usuario no autenticado');
      }
  
      const userId = user.uid;
  
      // Eliminar el registro de la base de datos
      await this.db.object(`${this.basePath}/${userId}/cultivos/${nuevoCultivoId}`).remove();
    } catch (error) {
      console.error('Error al eliminar la etapa:', error);
      throw error;
    }
  }

  async deleteEtapa(nuevaEtapaId: string): Promise<void> {
    try {
      const user = await this.afAuth.currentUser;
  
      if (!user) {
        throw new Error('Usuario no autenticado');
      }
  
      const userId = user.uid;
  
      // Eliminar el registro de la base de datos
      await this.db.object(`${this.basePath}/${userId}/etapas/${nuevaEtapaId}`).remove();
    } catch (error) {
      console.error('Error al eliminar la etapa:', error);
      throw error;
    }
  }
  
  

}// Final del export class Firebase
