import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MainUserComponent } from './components/main-user/main-user.component';
import { MainPorfileComponent } from './components/main-porfile/main-porfile.component';
import { MainPlantsComponent } from './components/main-plants/main-plants.component';
import { MainVideoComponent } from './components/main-video/main-video.component';
import { MainEtapaCultivoComponent } from './components/main-etapa-cultivo/main-etapa-cultivo.component';
import { MainNuevoCultivoComponent } from './components/main-nuevo-cultivo/main-nuevo-cultivo.component'; 
import { VerificacionComponent } from './components/verificacion/verificacion.component';
import { EmailVerificationGuard } from './components/email-verification';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'user', component: MainUserComponent,canActivate: [EmailVerificationGuard] },
  { path: 'porfile', component:MainPorfileComponent,canActivate: [EmailVerificationGuard] },
  { path: 'plants', component:MainPlantsComponent,canActivate: [EmailVerificationGuard] },
  { path: 'videos', component: MainVideoComponent,canActivate: [EmailVerificationGuard] },
  { path: 'etapacultivo', component: MainEtapaCultivoComponent,canActivate: [EmailVerificationGuard] },
  { path: 'nuevocultivo', component: MainNuevoCultivoComponent,canActivate: [EmailVerificationGuard] },
  { path:'verificar-correo', component:VerificacionComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
