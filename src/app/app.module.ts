import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MainUserComponent } from './components/main-user/main-user.component';
import { MainPorfileComponent } from './components/main-porfile/main-porfile.component';
import { MainPlantsComponent } from './components/main-plants/main-plants.component';
import { MainVideoComponent } from './components/main-video/main-video.component';
import { AppRoutingModule } from './app-routing.module';
import { MainNuevoCultivoComponent } from './components/main-nuevo-cultivo/main-nuevo-cultivo.component';
import { MainEtapaCultivoComponent } from './components/main-etapa-cultivo/main-etapa-cultivo.component';

// modulos importados
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { environment } from '../environments/environment';
import { VerificacionComponent } from './components/verificacion/verificacion.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    MainUserComponent,
    MainPorfileComponent,
    MainPlantsComponent,
    MainVideoComponent,
    MainNuevoCultivoComponent,
    MainEtapaCultivoComponent,
    VerificacionComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
