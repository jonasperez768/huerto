export interface Etapa {
  id:string;
  idcultivo: string;
  fecha: string;
  nombreEtapa: string;
  observaciones: string;
  medidas: string;
  horasCalor: string;
  tierra: string;
  ubicacion: string;
  substrato: string;
  imagenUrl: string;
     // Agrega la propiedad 'editable'
     editable?: boolean;
}
