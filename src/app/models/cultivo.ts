export interface Cultivo {
  id: string ;
  fecha: string;
  nombreComun: string;
  nombreCientifico: string;
  descripcion: string;
  crecimiento: string;
  luzt: string;
  tierra: string;
  ubicacion: string;
  substrato: string;
  imagenUrl: string;
  // Agrega la propiedad 'editable'
  editable?: boolean;
}
